#!/bin/bash

infile="$1"
k_level="$2"
k_pct="${2}%"
outfile="${k_pct} ${infile}_k${k_level}.png"

convert $infile -threshold $outfile
# echo $infile
# echo $outfile