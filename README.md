# vectormess

A tool to vectorize color images

Here's the idea-- given a color image:

- optionally resize to set size or proportions
- add borders or dots in the corners
- split into seperate RGB channels
- for each channel, posterize into n levels
  - n can be determined by arbitrary/fixed vals, or
  - fitting a curve to the histogram and finding minima, maixima, etc
- recombine each channel into one svg image with gray corresponding to level position
- combine channels into one svg and/or bitmap after rasterizing
