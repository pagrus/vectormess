#!/usr/bin/env python3

from matplotlib import pyplot as plt 
import numpy as np
import imageio

img = imageio.imread('kqed_selfie_320_g.png')

valrange = 255
degs = 30
flatimg = img.flatten()

hist,bins = np.histogram(flatimg, bins=valrange)

# plt.hist(flatimg, bins=valrange) 
# plt.title("histogram") 
# plt.show()

xvals = [ x for x in range(valrange) ]
x = np.array(xvals)
y = np.array(hist)

p30 = np.poly1d(np.polyfit(x, y, degs))

def find_mins(func):
    crit = func.deriv().r
    r_crit = crit[crit.imag==0].real
    test = func.deriv(2)(r_crit) 

    x_min = r_crit[test>0]
    y_min = func(x_min)
    mins = {'x': x_min, 'y':y_min}
    
    return mins

minima = find_mins(p30)

# xp = np.linspace(0, 255, 100)
# _ = plt.plot(x, y, '.', alpha=0.2)
# _ = plt.plot(xp, p30(xp), '--', color='#ff00ff')
# _ = plt.plot(minima['x'], minima['y'], 'o', color="#ff0000" )
# plt.ylim(0,1000)

# plt.show()

print(minima['x'])
