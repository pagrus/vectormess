#!/bin/bash

infile="$1"
stamp="$2"
wdir="$3"
colorspace="$4"
channel="$5"

convert $infile -colorspace $colorspace -channel $channel -separate $wdir/vectormess_separate_CMYK_${channel}_$stamp.png